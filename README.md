This repository holds the source code for the simulations performed to analyse the centromeric nucleosomes effects on the loop extrusion by LEs. It was developed in the research group Domestication Genomics at the Leibniz Institute of Plant Genetics and Crop Research (IPK) Gatersleben link.

In case of questions, please contact Amanda Souza Câmara (amandasc89@gmail.com).