# (c) 2018 IPK. All Rights Reserved
# Code written by: Amanda Souza Camara (camara@ipk-gatersleben.de)
# Based on Maksim Imakaev's scripts (from mirny lab)
from __future__ import print_function
from __future__ import division
import sys
import numpy as np 
from openmmlib import Simulation
import polymerutils
import os
import simtk.unit as units
import random
import bondsPredictor11
import pickle
import joblib

nm = units.meter * 1e-9
fs = units.second * 1e-15
ps = units.second * 1e-12

N = 100000   # number of monomers
nLEFI = int(sys.argv[1]) # this is related to separation of condensins I
nLEFII = int(sys.argv[2])
nLEFs = nLEFI + nLEFII
# parameters for the SMC bonds. The same proportion used in the loopExtrusion script from Maksim.
wiggleDist = 2 # Average displacement from the equilibrium bond distance
bondLength = 5 # The length of the SMC bonds
# bond dictionaries parameters
extrusionSteps = 100000
consideredSteps = 100000
saveBlock = [False]*(extrusionSteps+1) # save one conformation each #saveBlock
firstBlocks = 0 # how many blocks to wait before loop extrusion begins
waitBlocks = 0 # how many blocks to wait after extrusion of condensins II to add condensins I
partialBlocks = 50 # how many blocks per initialized simulation
prophaseBlocks = 0 # how many blocks to run after all loops of condensin I are extruded
finalBlocks = 0
totalBlocks = firstBlocks + extrusionSteps + finalBlocks
totalParts = int(totalBlocks/partialBlocks)
# creating a starting conformation
oneVolume = 178479 #1331 #the volume occupied by one nucleosome, in cubic nm
totalVolume = N*oneVolume
#dens = 0.00075
#box = (N / dens) ** 0.33  # density = 0.1.
# spherical confinement
radius = (totalVolume*3/(4*np.pi))**(1/3)
#print("spherical confinament radius",radius)


#data = polymerutils.grow_rw(N, int(box) - 2)  # creates a compact conformation
data = polymerutils.create_random_walk(1, N) # Creates an extended "random walk" conformation of length N
# uploads a conformation from file
#if meta==1:
#backData = joblib.load("/home/camara/myPol/holocentric/trajectory_loopExtrusion_setImodel1/block100000.dat")
#data = np.array(backData['data'])
#block = 50000
block=0
folder = str(sys.argv[3])
if not os.path.exists(folder):
 os.mkdir(folder)
 print("Directory", folder, " created.")
else:
 print("Directory ", folder, "already exists")

pi=np.pi

#distribution = [0,0,0,0,0,0,0,0,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
#distribution = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
if int(sys.argv[6])==0: distribution = [100]
if int(sys.argv[6])==1: distribution = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,0,0,0,0,0,0,0,0,0,0,0,0,0]
if int(sys.argv[6])==2: distribution = [0]
#distribution=[1000]
#distribution=[0]

size = N/len(distribution)
centromeres = []
for i in range(len(distribution)): centromeres = centromeres + np.int_(size*(i+np.random.random(distribution[i]))).tolist()
print (centromeres, len(centromeres))
ncentromeres = len(centromeres)

print("this simulation accounts for %d nucleosomes, being %d centromeric, and %d condensins I and %d condensis II." % (N,ncentromeres,nLEFI,nLEFII))

# list of save blocks
#for i in range(10000,11000,10): saveBlock[i]=True
#for i in range(30000,31000,100): saveBlock[i]=True
for i in range(29000,30000,100): saveBlock[i]=True
for i in range(0,extrusionSteps+1,1000): saveBlock[i]=True

# list of lifetimes
lifetime = []
for i in range(nLEFII): lifetime.append(int(sys.argv[5]))
for i in range(nLEFI): lifetime.append(int(sys.argv[4]))

#list of first binding time
birth = []
for i in range(nLEFII): birth.append(10000)
for i in range(nLEFI): birth.append(10000)


# list of LEFs being anchored by centromeres
centromereAnchor=[]
for i in range(nLEFII): centromereAnchor.append(1)
for i in range(nLEFI): centromereAnchor.append(0)
   
passElements = consideredSteps*nLEFs*2
centromeres = np.array(centromeres)+1
bondDict = bondsPredictor11.extruder(N,passElements,extrusionSteps,lifetime,birth,centromereAnchor,centromeres,nLEFs,ncentromeres)
centromeres = centromeres - 1
extrusionSteps = consideredSteps
print(bondDict, bondDict.shape)
loopBondDict = np.zeros((extrusionSteps,nLEFs,5))
loopBondDict[:,:,0:2] = bondDict.reshape(extrusionSteps,nLEFs,2) - 1
loopBondDict = loopBondDict.astype(int)
print(loopBondDict.shape)
print("average loop size is %d." % np.average([loopBondDict[extrusionSteps-1,:,1]-loopBondDict[extrusionSteps-1,:,0]]))
outfile = open("%s/loopsize.dat" % folder, 'w')
for i in range(extrusionSteps):
 outfile.write("%5.2f\n" % np.average(loopBondDict[i,:nLEFII,1]-loopBondDict[i,:nLEFII,0]))
outfile.close()

# saving special monomers final indexes to file
centromeres = np.array(centromeres)
pickle.dump(centromeres, open(os.path.join(folder, "centromeres.dat"),'wb'),protocol=2)
pickle.dump(loopBondDict[extrusionSteps-1,nLEFII:,0:2], open(os.path.join(folder, "condensinI.dat"),'wb'),protocol=2)
condensinII = loopBondDict[extrusionSteps-1,:nLEFII,0:2]
pickle.dump(loopBondDict[extrusionSteps-1,:nLEFII,0:2], open(os.path.join(folder, "condensinII.dat"),'wb'),protocol=2)


# do the loop extrusion => activates and inactivates bonds changing the value of k.
class smcMilker:
 def step(self, context, lBD, part, b, k, smcBondLength,saveFile):
  print("milking",part,b)
  for i in range(extrusionSteps):
   if lBD[i,0,2]==part and lBD[i,0,3]==b :
    for h in range (nLEFs):
     if lBD[i,h,0]!=-1: a.forceDict["HarmonicBondForce"].setBondParameters(int(lBD[i,h,4]), int(lBD[i,h,0]), int(lBD[i,h,1]), smcBondLength, k)
     #print(lBD[i,h,0:2])
   elif lBD[i,0,2]==part and lBD[i,0,3]!=b:
    for h in range (nLEFs):
     if lBD[i,h,0]!=-1:a.forceDict["HarmonicBondForce"].setBondParameters(int(lBD[i,h,4]), int(lBD[i,h,0]), int(lBD[i,h,1]), smcBondLength, 0)
  a.forceDict["HarmonicBondForce"].updateParametersInContext(context)

 def initialize(self, lBD,part,k,smcBondLength):
  print('setting up SMC bonds for this part',part )
  for b in range (extrusionSteps):
   if lBD[b,0,2]==part:
    for h in range (nLEFs):
     if lBD[b,h,0]!=-1: lBD[b,h,4] = a.forceDict["HarmonicBondForce"].addBond(int(lBD[b,h,0]),int(lBD[b,h,1]),smcBondLength,0)
  numBonds = a.forceDict["HarmonicBondForce"].getNumBonds()
  print("This simulation accounts now for %d bonds" % numBonds)

milker = smcMilker()

Epot = []
time = []
Ehand = []

print("Extrusion block steps are: %d" % (extrusionSteps))
blockRounds = firstBlocks+extrusionSteps++finalBlocks
print("The entire process will be over after %d blocks" % blockRounds)
totalParts = blockRounds//partialBlocks
for i in range(extrusionSteps):
 currentPart = (firstBlocks+i)//partialBlocks
 currentBlock = int((firstBlocks+i)%partialBlocks)
 #print(i,currentPart,currentBlock,loopBondDict[i,:,0:2])
 loopBondDict[i,:,2] = currentPart
 loopBondDict[i,:,3] = currentBlock

# run the loop extrusion simulation in parts so it won't slow down with too many initialized bonds.
for part in range(totalParts):
 print("Starting part number %d" % (part))

 a = Simulation(thermostat=0.01)
 a.setup(platform="cuda", integrator="variableLangevin", errorTol=0.001, precision="mixed")
 a.load(data)  # loads a polymer, puts a center of mass at zero
 a.saveFolder(folder)

 a.addHarmonicPolymerBonds(wiggleDist=1, bondLength=10) # the same proportion used in the Maksim's loopExtrusion script
 a.addPolynomialRepulsiveForce(trunc=5, radiusMult=10.5)
 a.addGrosbergStiffness(k=1)
 a.addSphericalConfinement(r=radius,k=5.)
# K is more or less arbitrary, k=4 corresponds to presistence length of 4,
# k=1.5 is recommended to make polymer realistically flexible; k=8 is very stiff

 k = a.kbondScalingFactor / (wiggleDist ** 2) # harmonic force constant
 smcBondLength = bondLength * a.length_scale

 numBonds = a.forceDict["HarmonicBondForce"].getNumBonds()
 print("There are %d polymer bonds." % numBonds)

 milker.initialize(loopBondDict,part,k,smcBondLength)
 numBonds = a.forceDict["HarmonicBondForce"].getNumBonds()
 print(numBonds)

# -----------Running a simulation ---------
 a.energyMinimization(stepsPerIteration=100)
 #a.doBlock(10)
 a.step = block
 a.step = block
 if block==0: a.save()
 for b in range(partialBlocks):  # Do #blockRounds blocks
  saveFile = False
  if saveBlock[block]: a.save()
  milker.step(a.context,loopBondDict,part,b,k,smcBondLength,saveFile)
  a.doBlock(100)  # Of 500 timesteps each

  Epot.append(a.state.getPotentialEnergy() / a.N / a.kT)
  time.append(a.state.getTime() / ps)
  
  data = a.getData()
  block = a.step

a.save()
outfile = open('%s/Epot.txt' % folder ,'w')
for i in range (len(Epot)):
 outfile.write("%5.2f\n" % Epot[i])
outfile.close()
