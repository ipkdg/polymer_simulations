# (c) 2018 IPK. All Rights Reserved
# Code written by: Amanda Souza Camara (camara@ipk-gatersleben.de)
# Based on Maksim Imakaev's scripts (from mirny lab)
from __future__ import print_function
from __future__ import division
import sys
import numpy as np 
from openmmlib_1 import Simulation
import polymerutils
import os
import simtk.unit as units
import random
import bondsPredictor11
import pickle
import joblib

def find_nearest(array, value):
 array = np.asarray(array)
 idx = (np.abs(array - value)).argmin()
 return array[idx]

nm = units.meter * 1e-9
fs = units.second * 1e-15
ps = units.second * 1e-12

N = 100000   # number of monomers
nLEFI = int(sys.argv[1]) # this is related to separation of condensins I
nLEFII = int(sys.argv[2])
nLEFs = nLEFI + nLEFII
# parameters for the SMC bonds. The same proportion used in the loopExtrusion script from Maksim.
wiggleDist = 2 # Average displacement from the equilibrium bond distance
bondLength = 5 # The length of the SMC bonds
# bond dictionaries parameters
extrusionSteps = 100000
consideredSteps = 50000
saveBlock = [False]*(extrusionSteps+1) # save one conformation each #saveBlock
firstBlocks = 0 # how many blocks to wait before loop extrusion begins
waitBlocks = 0 # how many blocks to wait after extrusion of condensins II to add condensins I
partialBlocks = 50 # how many blocks per initialized simulation
prophaseBlocks = 0 # how many blocks to run after all loops of condensin I are extruded
finalBlocks = 0
totalBlocks = firstBlocks + extrusionSteps + finalBlocks
totalParts = int(totalBlocks/partialBlocks)
# creating a starting conformation
oneVolume = 178479 #1331 #the volume occupied by one nucleosome, in cubic nm
totalVolume = N*oneVolume
#dens = 0.00075
#box = (N / dens) ** 0.33  # density = 0.1.
# spherical confinement
radius = (totalVolume*3/(4*np.pi))**(1/3)
print("spherical confinement radius",radius)


#data = polymerutils.grow_rw(N, int(box) - 2)  # creates a compact conformation
#data = polymerutils.create_random_walk(1, N) # Creates an extended "random walk" conformation of length N
# uploads a conformation from file
#if meta==1:
#backData = joblib.load("/home/camara/myPol/holocentric/trajectory_loopExtrusion_setImodel1/block100000.dat")
#data = np.array(backData['data'])
#block = 50000
block=0
folder = str(sys.argv[3])
if not os.path.exists(folder):
 os.mkdir(folder)
 print("Directory", folder, " created.")
else:
 print("Directory ", folder, "already exists")

pi=np.pi

distribution = [100]
size = N/len(distribution)
centromeres = []
#for i in range(len(distribution)): centromeres = centromeres + np.int_(size*(i+np.random.random(distribution[i]))).tolist()
centromeres = [492,1535,3343,4000,4776,5484,6279,7683,10930,11381,12498,13615,13853,15355,16350,17691,18178,19628,19794,22035,22097,22903,25708,26059,27762,29978,31603,31779,32158,32477,32587,33320,33480,35001,36566,37472,37708,38765,39344,40376,40450,40762,43436,43504,43820,45838,46095,46488,46559,46946,47065,47480,49102,49599,52473,54556,55121,58350,60747,65473,65532,69079,69555,70804,70812,71444,71557,71694,72302,73494,73896,74080,74610,74807,74834,76935,76958,77981,78055,79310,81483,81719,83843,84382,84925,85077,86016,86574,87298,87421,91748,94477,94869,95540,95693,97013,97235,98138,98717,99793]
print (centromeres, len(centromeres))
ncentromeres = len(centromeres)


# list of save blocks
for i in range(10000,11000,100): saveBlock[i]=True
for i in range(49000,50000,100): saveBlock[i]=True
for i in range(0,extrusionSteps+1,1000): saveBlock[i]=True

# list of lifetimes
lifetime = []
for i in range(nLEFII): lifetime.append(int(sys.argv[5]))
for i in range(nLEFI): lifetime.append(int(sys.argv[4]))

#list of first binding time
birth = []
for i in range(nLEFII): birth.append(10000)
for i in range(nLEFI): birth.append(30000)

# list of LEFs being anchored by centromeres
centromereAnchor=[]
for i in range(nLEFII): centromereAnchor.append(1)
for i in range(nLEFI): centromereAnchor.append(0)

passElements = consideredSteps*nLEFs*2
centromeres = np.array(centromeres)+1
bondDict = bondsPredictor11.extruder(N,passElements,extrusionSteps,lifetime,birth,centromereAnchor,centromeres,nLEFs,ncentromeres)
print("back to python")
#with open("loopBondDict.bin","rb") as f:
# byte = f.read(1)
# while byte !="":
#  byte = f.read(1)
#  print(byte)
extrusionSteps = consideredSteps
centromeres = centromeres - 1
print(bondDict, bondDict.shape)
loopBondDict = np.zeros((extrusionSteps,nLEFs,5))
loopBondDict[:,:,0:2] = bondDict.reshape(extrusionSteps,nLEFs,2) - 1
loopBondDict = loopBondDict.astype(int)
print(loopBondDict.shape)
print("average loop size is %d." % np.average([loopBondDict[extrusionSteps-1,:,1]-loopBondDict[extrusionSteps-1,:,0]]))
outfile = open("%s/loopsize.dat" % folder, 'w')
for i in range(extrusionSteps):
 outfile.write("%5.2f\n" % np.average(loopBondDict[i,:nLEFII,1]-loopBondDict[i,:nLEFII,0]))
outfile.close()

# saving special monomers final indexes to file
centromeres = np.sort(np.array(centromeres))
pickle.dump(centromeres, open(os.path.join(folder, "centromeres.dat"),'wb'),protocol=2)
pickle.dump(loopBondDict[extrusionSteps-1,nLEFII:,0:2], open(os.path.join(folder, "condensinI.dat"),'wb'),protocol=2)
condensinII = loopBondDict[extrusionSteps-1,:nLEFII,0:2]
pickle.dump(condensinII, open(os.path.join(folder, "condensinII.dat"),'wb'),protocol=2)


# do the loop extrusion => activates and inactivates bonds changing the value of k.
class smcMilker:
 def step(self, context, lBD, part, b, k, smcBondLength,saveFile):
  print("milking",part,b)
  for i in range(extrusionSteps):
   if lBD[i,0,2]==part and lBD[i,0,3]==b :
    for h in range (nLEFs):
     if lBD[i,h,0]!=-1: a.forceDict["HarmonicBondForce"].setBondParameters(int(lBD[i,h,4]), int(lBD[i,h,0]), int(lBD[i,h,1]), smcBondLength, k)
     #print(lBD[i,h,0:2])
   elif lBD[i,0,2]==part and lBD[i,0,3]!=b:
    for h in range (nLEFs):
     if lBD[i,h,0]!=-1:a.forceDict["HarmonicBondForce"].setBondParameters(int(lBD[i,h,4]), int(lBD[i,h,0]), int(lBD[i,h,1]), smcBondLength, 0)
  a.forceDict["HarmonicBondForce"].updateParametersInContext(context)

 def initialize(self, lBD,part,k,smcBondLength):
  print('setting up SMC bonds for this part',part )
  for b in range (extrusionSteps):
   if lBD[b,0,2]==part:
    for h in range (nLEFs):
     if lBD[b,h,0]!=-1: lBD[b,h,4] = a.forceDict["HarmonicBondForce"].addBond(int(lBD[b,h,0]),int(lBD[b,h,1]),smcBondLength,0)
  numBonds = a.forceDict["HarmonicBondForce"].getNumBonds()
  print("This simulation accounts now for %d bonds" % numBonds)

milker = smcMilker()

Epot = []
time = []
Ehand = []

print("Extrusion block steps are: %d" % (extrusionSteps))
blockRounds = firstBlocks+extrusionSteps++finalBlocks
print("The entire process will be over after %d blocks" % blockRounds)
totalParts = blockRounds//partialBlocks
for i in range(extrusionSteps):
 currentPart = (firstBlocks+i)//partialBlocks
 currentBlock = int((firstBlocks+i)%partialBlocks)
 #print(i,currentPart,currentBlock,loopBondDict[i,:,0:2])
 loopBondDict[i,:,2] = currentPart
 loopBondDict[i,:,3] = currentBlock

# create kinetochores
chromatin=N
kx = int(sys.argv[6]) 
ky = int(sys.argv[7]) # kinetochore beads per centromere beads
kz = int(sys.argv[8])
nkinetochores=ncentromeres*kz*kx*ky
N=chromatin+nkinetochores
chains = [(0,chromatin,0)] # list of tuples, [(start,end,is ring),(...]
for i in range(nkinetochores):
 kinetochore = chromatin + i
 chains = chains + [(kinetochore,kinetochore,0)]
# print (chains)
# indexing
#k=[0]
#for i in range(kx*ky):
# k[i]=range(chromatin+i,N,ksize)
kinetochore = np.arange(chromatin,N)
# tethering kinetochores
kl = 10 # kinetochore monomers are separated by 5 nm in y and in x
kh = 10 # kinetochore chains are separated by 5 nm in z
y0 = -kl*(ky//2)
print("y0",y0)
kinetoPositions=np.zeros((nkinetochores,3))
for i in range(nkinetochores):
 kinetoPositions[i,2] = kh*(i//(kx*ky))
 kinetoPositions[i,1] = y0 + kl*(i//kx%ky)
 kinetoPositions[i,0] = -kl*(i%kx)
 print(i,kinetoPositions[i,0],kinetoPositions[i,1],kinetoPositions[i,2])
#for i in range(ky):
# k[i,:,1] = y0 + i*kl
#for i in range(nkinetochores):
# k[:,i,2] = kh*i
#kinetoPositions = np.concatenate((k))
print("kinetochores",kinetochore,kinetoPositions)
# tethering centromeres to the side of the kinetochores
centromerePositions = np.zeros((ncentromeres,3))
for i in range(ncentromeres):
 centromerePositions[i,:] = [10,0,kz*kh*i]
print("centromeres",centromeres,centromerePositions)
kinetoCentro = np.concatenate((kinetochore,centromeres))
kinetoCentroPositions = np.concatenate((kinetoPositions,centromerePositions))

# building data[N,3], initial conformation
# fixing centromeres, condensins, monomer 0 and monomer N
print("N", N, chromatin)
data = np.zeros((N,3))
condensinMonomers = np.sort(loopBondDict[0,:,:2].reshape(nLEFs*2))
print("condensinMonomers", len(condensinMonomers),condensinMonomers)
for i in range(ncentromeres): 
 data[centromeres[i],:] = centromerePositions[i,:]
 #print(data[centromeres[i],:])
centroRef = [0] + centromeres.tolist() + [N]
data[0,:] = [10,0,-kz*kh]
data[chromatin-1,:] = [10,0,kz*kh*(ncentromeres+1)]
for i in range(len(centroRef)-1):
 condensinsBetweenCentromeres = condensinMonomers[(condensinMonomers>=centroRef[i])&(condensinMonomers<=centroRef[i+1])]
 print(centroRef[i],centroRef[i+1],condensinsBetweenCentromeres)
 nc = len(condensinsBetweenCentromeres)
 d = kz*kh/(nc+1)
 for j in range(nc):
  data[condensinsBetweenCentromeres[j],:] = [10,0,data[centroRef[i],2]+(j+1)*d]
  print(condensinsBetweenCentromeres[j],data[condensinsBetweenCentromeres[j],:])
# assigning coordinates to the rest of chromatin as loops between the monomeres already assigned
baseMonomers = np.sort(condensinMonomers.tolist()+centroRef)
print("baseMonomers",len(baseMonomers))
for i in range(chromatin):
 if not any(i==b for b in baseMonomers):
  base = find_nearest(baseMonomers,i)
  if base>i : data[i,:] = [10+(base-i),0,data[base,2]-kh/3]
  if base<i : data[i,:] = [10+(i-base),0,data[base,2]+kh/3]
for i in range(nkinetochores): data[kinetochore[i],:]=kinetoPositions[i,:]
print("first monomer", data[0,:])

# run the loop extrusion simulation in parts so it won't slow down with too many initialized bonds.
for part in range(totalParts):
 print("Starting part number %d" % (part))

 a = Simulation(thermostat=0.01)
 a.setup(platform="cuda", integrator="variableLangevin", errorTol=0.001, precision="mixed")
 a.load(data)  # loads a polymer, puts a center of mass at zero
 a.saveFolder(folder)

 a.setChains(chains=chains)

 a.addHarmonicPolymerBonds(wiggleDist=1, bondLength=10) # the same proportion used in the Maksim's loopExtrusion script
 # adding repulsive force with different truncation for chromatin and kinetochores
 trunc = [5]*chromatin + [10000]*(N-chromatin)
 radius =[10.5]*N
 #radius = 10.5
 selection = range(N)
 a.addPolynomialRepulsiveForceSelection(trunc=trunc, radius=radius)
 #a.addPolynomialRepulsiveForce()
 a.addGrosbergStiffness(k=1)

#a.addSphericalConfinement(r=radius,k=5.)
# K is more or less arbitrary, k=4 corresponds to presistence length of 4,
# k=1.5 is recommended to make polymer realistically flexible; k=8 is very stiff

# a.tetherParticles(kinetochore,k=10,positions=kinetoPositions)
# a.tetherParticles(centromeres,k=10,positions=centromerePositions)
 a.tetherParticles(kinetoCentro,k=10,positions=kinetoCentroPositions)

 k = a.kbondScalingFactor / (wiggleDist ** 2) # harmonic force constant
 smcBondLength = bondLength * a.length_scale

 numBonds = a.forceDict["HarmonicBondForce"].getNumBonds()
 print("There are %d polymer bonds." % numBonds)

 milker.initialize(loopBondDict,part,k,smcBondLength)
 numBonds = a.forceDict["HarmonicBondForce"].getNumBonds()
 print(numBonds)

 if block==0: a.save()
# -----------Running a simulation ---------
 a.energyMinimization(stepsPerIteration=100)
 #a.doBlock(10)
 a.step = block
 for b in range(partialBlocks):  # Do #blockRounds blocks
  saveFile = False
  if saveBlock[block] and block>0: a.save()
  milker.step(a.context,loopBondDict,part,b,k,smcBondLength,saveFile)
  a.doBlock(100)  # Of 500 timesteps each

  Epot.append(a.state.getPotentialEnergy() / a.N / a.kT)
  time.append(a.state.getTime() / ps)
  
  data = a.getData()
  block = a.step

a.save()
outfile = open('%s/Epot.txt' % folder ,'w')
for i in range (len(Epot)):
 outfile.write("%5.2f\n" % Epot[i])
outfile.close()
