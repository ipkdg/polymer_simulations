# (c) 2018 IPK. All Rights Reserved
# Code written by: Amanda Souza Camara (camara@ipk-gatersleben.de)
# Based on Maksim Imakaev's scripts (from mirny lab)
from __future__ import print_function
from __future__ import division
import sys
import numpy as np 
from openmmlib import Simulation
import polymerutils
import os
import simtk.unit as units
import random
import bondsPredictor11
import pickle
import joblib

nm = units.meter * 1e-9
fs = units.second * 1e-15
ps = units.second * 1e-12

N = 100000   # number of monomers
nLEFI = int(sys.argv[1]) # this is related to separation of condensins I
nLEFII = int(sys.argv[2])
nLEFs = nLEFI + nLEFII
# bond dictionaries parameters
extrusionSteps = 50000
consideredSteps = 50000

folder = str(sys.argv[3])
if not os.path.exists(folder):
 os.mkdir(folder)
 print("Directory", folder, " created.")
else:
 print("Directory ", folder, "already exists")

pi=np.pi

#distribution = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
if int(sys.argv[6])==0:
 distribution = [100]
 regions = False
if int(sys.argv[6])==1:
 distribution = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,0,0,0,0,0,0,0,0,0,0,0,0,0]
 regions = True
if int(sys.argv[6])==2:
 distribution = [0]
 regions = False

size = N/len(distribution)
centromeres = []
for i in range(len(distribution)): centromeres = centromeres + np.int_(size*(i+np.random.random(distribution[i]))).tolist()
print (centromeres, len(centromeres))
ncentromeres = len(centromeres)


# list of lifetimes
lifetime = []
for i in range(nLEFII): lifetime.append(int(sys.argv[5]))
for i in range(nLEFI): lifetime.append(int(sys.argv[4]))

#list of first binding time
birth = []
for i in range(nLEFII): birth.append(10000)
for i in range(nLEFI): birth.append(10000)

# list of LEFs being anchored by centromeres
centromereAnchor=[]
for i in range(nLEFII): centromereAnchor.append(1)
for i in range(nLEFI): centromereAnchor.append(0)

passElements = consideredSteps*nLEFs*2
centromeres = np.array(centromeres)+1
bondDict = bondsPredictor11.extruder(N,passElements,extrusionSteps,lifetime,birth,centromereAnchor,centromeres,nLEFs,ncentromeres)
#bondDict = np.zeros((consideredSteps,nLEFs,2))
extrusionSteps = consideredSteps
print("back to python. Loop stats for last ", extrusionSteps)
centromeres = np.sort(centromeres) - 1
print(bondDict.shape,bondDict)
#for i in range(len(bondDict)): print(bondDict[i])
#loopBondDict = np.zeros((extrusionSteps,nLEFs,2))
loopBondDict = bondDict.reshape(extrusionSteps,nLEFs,2)-1
print(loopBondDict[extrusionSteps-1,:,:])
#loopBondDict = loopBondDict.astype(int)
#print(loopBondDict.shape,loopBondDict)
#for i in range(extrusionSteps):
#print(loopBondDict[extrusionSteps-1,0,:])
run = int(sys.argv[7])
# calculating loop size
#print("final average loop size is %d." % np.average([loopBondDict[extrusionSteps-1,:,1]-loopBondDict[extrusionSteps-1,:,0]]))

if not regions:
 loopsize=[]
 filename = "50000loopsize_" + str(run) + ".dat"
 print(os.path.join(folder,filename))
 #print(loopBondDict[0,:,0:2])
 for i in range(extrusionSteps):
  #print(i, "loopsize step",loopBondDict[i,:,0])
  if loopBondDict[i,0,0]!=-1:
   #print(i,"entered if",loopBondDict[i,:,0]!=-1)
   firstpos = loopBondDict[i,:,0][loopBondDict[i,:,0]!=-1]
   lastpos = loopBondDict[i,:,1][loopBondDict[i,:,0]!=-1]
   #print(i,np.average(lastpos-firstpos),firstpos.size,firstpos[0],lastpos[0])
   if firstpos.size>0:loopsize.append(np.average(lastpos-firstpos))
   if firstpos.size==0:loopsize.append(0)
   #print(i,loopsize[i])
  else:
   loopsize.append(0)
  print(i,loopsize[i]) 
 pickle.dump(loopsize, open(os.path.join(folder,filename),'wb'),protocol=2)


if regions:
 loopsize=[]
 filename = "50000loopsizea_" + str(run) + ".dat"
 print(os.path.join(folder,filename))
 #print(loopBondDict[0,:,0:2])
 for i in range(extrusionSteps):
  #print(i, "loopsize step")
  if loopBondDict[i,0,0]!=-1: 
   firstpos = loopBondDict[i,:,0][(loopBondDict[i,:,0]>centromeres[0]) & (loopBondDict[i,:,1]<centromeres[-1])]
   lastpos = loopBondDict[i,:,1][(loopBondDict[i,:,0]>centromeres[0]) & (loopBondDict[i,:,1]<centromeres[-1])]
   #print(i,np.average(lastpos-firstpos),firstpos.size,firstpos[0],lastpos[0])
   if firstpos.size>0:loopsize.append(np.average(lastpos-firstpos))
   if firstpos.size==0:loopsize.append(0)
  else:
   loopsize.append(0)
  print(i,loopsize[i])
 pickle.dump(loopsize, open(os.path.join(folder,filename),'wb'),protocol=2)
 
  # calculating loop size for a different region
 loopsize=[]
 filename = "50000loopsizeb_" + str(run) + ".dat"
 print(os.path.join(folder,filename))
 #print(loopBondDict[0,:,0:2])
 for i in range(extrusionSteps):
 #print(i, "loopsize step")
  if loopBondDict[i,0,0]!=-1:
   firstpos = loopBondDict[i,:,0][(loopBondDict[i,:,0]!=-1) & ((loopBondDict[i,:,0]<centromeres[0]) | (loopBondDict[i,:,1]>centromeres[-1]))]
   lastpos = loopBondDict[i,:,1][(loopBondDict[i,:,0]!=-1) & ((loopBondDict[i,:,0]<centromeres[0]) | (loopBondDict[i,:,1]>centromeres[-1]))]
   #print(i,np.average(lastpos-firstpos),firstpos.size)
   loopsize.append(np.average(lastpos-firstpos))
  else:
   loopsize.append(0)
  print(i,loopsize[i])
 pickle.dump(loopsize, open(os.path.join(folder,filename),'wb'),protocol=2)
     #


# calculating shortest path, or chromosome length
spfilename = "50000lastshortestpath_" + str(run) + ".dat"
filename = "50000shortestdistance_" + str(run) + ".dat"
print(spfilename,filename)
sdlist=[]
for i in range (extrusionSteps):
 path=loopBondDict[i,:,:2][loopBondDict[i,:,0].argsort()]
 #print(path)
 #fakenest=path[:,1]-path[:,0]<10*np.array(lifetime)
 #path=path[fakenest]
 nest=path[1:,0]>path[:-1,1]
 print(path)
 print(nest)
 while not nest.all():
  path=np.concatenate((np.array([path[0,:]]),path[1:,:][nest]),axis=0)
  nest=path[1:,0]>path[:-1,1]
 path[:,1]=path[:,1]-1
 distance=N-np.sum(np.diff(path,axis=1))
 print(i,distance)
 sdlist.append(distance)
print(path)
pickle.dump(sdlist, open(os.path.join(folder, filename),'wb'),protocol=2)
pickle.dump(path, open(os.path.join(folder, spfilename),'wb'),protocol=2)



